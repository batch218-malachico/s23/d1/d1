// console.log("Hello World!")
/*SECTION Objects*/

/*
    - An object is a data type that is used to represent real world objects
    - It is a collection of related data and/or functionalities
    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
    - Information stored in objects are represented in a "key:value" pair
    - A "key" is also mostly referred to as a "property" of an object
- Different data types may be stored in an object's property creating complex data structures
*/

/*Creating objects using object initializers/literal notation*/

/*
    - This creates/declares an object and also initializes/assigns it's properties upon creation
    - A cellphone is an example of a real world object
    - It has it's own properties such as name, color, weight, unit model and a lot of other things
    - Syntax
        let objectName = {
            keyA: valueA,
            keyB: valueB
        };
*/
// let cellphone = ['Nokia 3210', 1999, 10000];

let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: 1999
};

console.log('Result from creating objects using initializers/literal notation:');
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function
/*
    - Creates a reusable function to create several objects that have the same data structure
    - This is useful for creating multiple instances/copies of an object
    - An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
    - Syntax
        function ObjectName(keyA, keyB) {
            this.keyA = keyA;
            this.keyB = keyB;
        }
*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters

function Laptop(name, manufactureDate){
    this.name = name;
    this.manufactureDate = manufactureDate;
}

/*This is a unique instance of the laptop object*/

let laptop = new Laptop("Asus X415", 2022);
console.log('Result from creating objects using object constructors');
console.log(laptop);

let myLaptop = new Laptop('MacBook Air', 2020);
console.log('Result from creating objects using object constructors');
console.log(myLaptop);

let oldLaptop = Laptop('Portal RZE CCMC', 1980);
console.log('Result from creating objects without the new keyword:');
console.log(oldLaptop);

let myNewLaptop = Laptop('Asus x415', 2000);
console.log('Result from creating objects new keyword:');
console.log(myNewLaptop);
// base on construction of function name then followed by m-date
/*make sure to use new keyword to store data / values successfully;*/

/*Creating empty objects*/
let computer = { };
let myComputer = new Object();

/*SECTION accessing object properties accesing name and manugfacture date*/

console.log("dot notation")


// Using dot notation
console.log("Result from that notation: " + myLaptop.name);
console.log("Result from that notation: " + cellphone.name);
console.log("Result from that notation: " + myLaptop.manufactureDate);


// using the square bracket notation
console.log("Result from square bracket notation: " + myLaptop['name']);
console.log("Result from that notation: " + cellphone['name']);

/*console.log("Result from square bracket notation: " + myNewLaptop['manufactureDate']);
*/
/*object inside an array*/
let array = [laptop, myLaptop];

console.log(array[0]['name']);
console.log(array[0].name);

// [Section] Initializing/Adding/Deleting/Reassigning Object Properties
/*
    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
    - This is useful for times when an object's properties are undetermined at the time of creating them
*/
console.log("=================================");
console.log("=> Initializing an object property")
let car = {};  /*declared empty*/

/*initialization using dot notation*/
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation");
console.log(car);

/*initializing using square bracket notation*/

car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['manufacture Date']); /*undefined because javascript is case sensitive*/
console.log(car.manufactureDate); /* undefined- it is much effective if we use square bracket to access property name with spaces*/

console.log("Result from adding properties using square bracket notation")
console.log(car);

/*Deleting object properties*/
delete car['manufacture date'];
console.log("Result from deleting properties");
console.log(car);

/*reassigning object properties*/
/*car.name = "Dodge Charger R/T";
console.log(car);
*/

/*reassigning through sqaure bracket notation*/
car['name'] = "Dodge Charger R/T";
console.log("Result from reassigning properties: ");
console.log(car);
/*we could reassign using dot and square bracket notation*/

// [Section] Object Methods
/*
    - A method is a function which is a property of an object
    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
    - Methods are useful for creating object specific functions which are used to perform tasks on them
    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
*/


// object methods - function inside an object property
let person = {
    name: 'John',

    // method
    talk: function(){
        console.log("Hello my name is " +this.name);
    } /*we use this keyword to access a property that co-exist inside*/
}

console.log(person);
console.log("Result from object methods: ");
person.talk();

/*adding methods to objects*/
// we could create a method outside the object code
// syntax for initializing a property/re-assigning/adding a property
// person - object
// walk - new property

// Initialize create new property
person.walk = function(){
    console.log(this.name+ ' walked 25 steps forward. ');
}
person.walk();

/*reassigning a property value*/
person.walk = function(){
    console.log(this.name+ ' walked 30 steps forward. ');
}
person.walk();
console.log(person);

console.log(person.walk);

// Methods are useful for creating reusable functions that performs tasks related to objects

/*
-Scenario

*/

function Pokemon(name, level){
    // properties
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = function(target){
        console.log(this.name + "tackled" + target.name);

        target.health -= this.attack
        console.log(target.name + "health is now reduced to " + target.health);

        if(target.health <= 0){
            target.faint();
        }
    }
    this.faint = function(){
        console.log(this.name + " fainted ")
    }
}

let pikachu = new Pokemon("Pikachu", 99);
console.log(pikachu);

let rattata = new Pokemon("Rattata", 5);
console.log(rattata);

pikachu.tackle(rattata);

rattata.tackle(pikachu);